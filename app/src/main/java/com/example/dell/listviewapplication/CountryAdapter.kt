package com.example.dell.listviewapplication

import android.support.v4.widget.ViewDragHelper

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.AbsListView
import kotlinx.android.synthetic.main.country_list_item.view.*

import java.text.FieldPosition


// * Created by Dell on 25/10/2018.
class CountryListAdapter(val items : ArrayList<String>, val flags :Array<Int>, val context:Context ):ResycelerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return item.size
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewTaype: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.country_list_item parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.tvAnimal Type ?. text = items . get (postion)
        holder?.imgviewflag?.setImageResource(flags.get(position))
    }


    class ViewHolder(view:View):AbsListView.RecyclerView.ViewHolder(view){

        val tvAnimalType = view.tv._country_show
        val imageFlag = view.imgv_flag
    }
}

